%global fontname nerd-fonts-fira-code
%global fontconf 60-%{fontname}.conf

Name:           %{fontname}
Version:        3.0.2
Release:        1%{?dist}
Summary:        Patched monospaced font Fira Code from the nerd-fonts library

License:        OFL
URL:            https://github.com/ryanoasis/nerd-fonts
Source0:        https://github.com/ryanoasis/nerd-fonts/releases/download/%{version}/FiraCode.zip#/%{name}-%{version}.zip
Source1:        %{fontname}-fontconfig.conf
Source2:        %{fontname}.metainfo.xml

BuildArch:      noarch
BuildRequires:  fontpackages-devel
BuildRequires:  libappstream-glib

Requires:       fontpackages-filesystem

%description
Fira Code is an extension of the Fira Mono font containing a set of ligatures
for common programming multi-character combinations. This is just a font
rendering feature: underlying code remains ASCII-compatible. This helps to
read and understand code faster. For some frequent sequences like .. or //,
ligatures allow us to correct spacing.


%prep
%autosetup -c


%build


%install
find . -xdev -maxdepth 2 -iname "*.ttf" -iname "*Windows Compatible.ttf" -print -delete
install -m 0755 -d %{buildroot}%{_datadir}/fonts/%{fontname}
install -m 0644 -p *.ttf %{buildroot}%{_datadir}/fonts/%{fontname}

install -m 0755 -d %{buildroot}%{_fontconfig_templatedir} \
                   %{buildroot}%{_fontconfig_confdir}

install -m 0644 -p %{SOURCE1} \
        %{buildroot}%{_fontconfig_templatedir}/%{fontconf}

ln -s %{_fontconfig_templatedir}/%{fontconf} \
        %{buildroot}%{_fontconfig_confdir}/%{fontconf}

# Add AppStream metadata file
install -Dm 0644 -p %{SOURCE2} \
        %{buildroot}%{_datadir}/metainfo/%{fontname}.metainfo.xml

%check
appstream-util validate-relax --nonet \
        %{buildroot}%{_datadir}/metainfo/%{fontname}.metainfo.xml

%files
%defattr(-,root,root,-)
%{_datadir}/fonts/%{fontname}
%{_datadir}/metainfo/%{fontname}.metainfo.xml
%{_fontconfig_templatedir}/%{fontconf}
%{_fontconfig_confdir}/%{fontconf}

%changelog
* Sat Oct 14 2023 wanderer <wanderer at dotya dot ml> - 3-0-2
* Fri Aug 12 2022 wanderer <a_mirre@utb.cz> - 2-1-0
- Initial package - based on https://src.fedoraproject.org/rpms/fira-code-fonts/tree/f36

## # this package is based on the official fedora fira-code-fonts package,
## # original changelog below:
##
## #* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 6.2-2
## #- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild
## #
## #* Tue Dec 07 2021 Michael Kuhn <suraia@fedoraproject.org> - 6.2-1
## #- Update to 6.2
## #
## #* Fri Dec 03 2021 Michael Kuhn <suraia@fedoraproject.org> - 6.1-1
## #- Update to 6.1
## #
## #* Mon Nov 29 2021 Michael Kuhn <suraia@fedoraproject.org> - 6-1
## #- Update to 6
## #
## #* Wed Jul 21 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.2-4
## #- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild
## #
## #* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 5.2-3
## #- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild
## #
## #* Mon Jul 27 2020 Fedora Release Engineering <releng@fedoraproject.org> - 5.2-2
## #- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild
## #
## #* Fri Jun 12 2020 Michael Kuhn <suraia@fedoraproject.org> - 5.2-1
## #- Update to 5.2
## #
## #* Thu Jun 11 2020 Michael Kuhn <suraia@fedoraproject.org> - 5.1-1
## #- Update to 5.1
## #
## #* Mon Jun 08 2020 Michael Kuhn <suraia@fedoraproject.org> - 5-1
## #- Update to 5
## #- Switch to TTF, see https://github.com/tonsky/FiraCode/issues/939
## #
## #* Mon May 18 2020 Michael Kuhn <suraia@fedoraproject.org> - 4-1
## #- Update to 4
## #
## #* Wed Apr 15 2020 Michael Kuhn <suraia@fedoraproject.org> - 3.1-1
## #- Update to 3.1
## #
## #* Fri Apr 10 2020 Michael Kuhn <suraia@fedoraproject.org> - 3-1
## #- Update to 3
## #
## #* Tue Jan 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 2-2
## #- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild
## #
## #* Thu Dec 05 2019 Michael Kuhn <suraia@fedoraproject.org> - 2-1
## #- Initial package
